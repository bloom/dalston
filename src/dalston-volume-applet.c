/*
 * Dalston - power and volume applets for Moblin
 * Copyright (C) 2009, Intel Corporation.
 *
 * Authors: Rob Bradford <rob@linux.intel.com>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU Lesser General Public License,
 * version 2.1, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St - Fifth Floor, Boston, MA 02110-1301 USA.
 *
 */


#include <gtk/gtk.h>
#include <glib/gi18n.h>

#include <dalston/dalston-volume-applet.h>

#include <moblin-panel/mpl-panel-common.h>
#include <moblin-panel/mpl-panel-gtk.h>
#include <config.h>

#define PKG_THEMEDIR PKG_DATADIR"/theme"

static MplPanelClient *panel_client;
static gboolean mouse_first_inside = FALSE;
static gboolean mouse_inside = FALSE;
static guint timer;

static gboolean
request_hide (gpointer data)
{
  MplPanelClient *panel_client = (MplPanelClient*) data;
  mpl_panel_client_request_hide(panel_client);
  mouse_first_inside = FALSE;
  gtk_timeout_remove(timer);

  return FALSE;
}

static gboolean
visibility_changed (GtkWidget          *widget,
                    GdkEventVisibility *event,
                    gpointer            user_data)
{
  if (event->state != GDK_VISIBILITY_UNOBSCURED && mouse_first_inside && !mouse_inside) {
     request_hide(panel_client);
  }
  else if (event->state == GDK_VISIBILITY_UNOBSCURED) {
     gtk_timeout_remove(timer);
     timer = gtk_timeout_add(5000, request_hide, panel_client);
  }

  return FALSE;
}

static gboolean
mouse_enter (GtkWidget        *widget,
             GdkEventCrossing *event,
             gpointer          user_data)
{
  if (!mouse_first_inside) mouse_first_inside = TRUE;
  mouse_inside = TRUE;
  gtk_timeout_remove(timer);

  return FALSE;
}

static gboolean
mouse_leave (GtkWidget        *widget,
             GdkEventCrossing *event,
             gpointer          user_data)
{
  gint x, y, width, height;
  gtk_widget_get_size_request(widget, &width, &height);
  gtk_widget_get_pointer(widget, &x, &y);
  if ( x < 0 || y < 0 || x > width || y > height) {
      mouse_inside = FALSE;
  }

  return FALSE;
}

int
main (int    argc,
      char **argv)
{
  DalstonVolumeApplet *volume_applet;
  GtkWidget *pane;
  GtkWidget *window;
  GtkSettings *settings;

  bindtextdomain (GETTEXT_PACKAGE, LOCALEDIR);
  bind_textdomain_codeset (GETTEXT_PACKAGE, "UTF-8");
  textdomain (GETTEXT_PACKAGE);

  gtk_init (&argc, &argv);

  /* Force to the moblin theme */
  settings = gtk_settings_get_default ();
  gtk_settings_set_string_property (settings,
                                    "gtk-theme-name",
                                    "Bloom",
                                    NULL);

  panel_client = mpl_panel_gtk_new (MPL_PANEL_VOLUME,
                                    _("sound"),
                                    PKG_THEMEDIR "/volume-applet.css",
                                    "unknown",
                                    TRUE);

  mpl_panel_client_set_size_request (panel_client, 400, 200);

  /* Volume applet */
  volume_applet = dalston_volume_applet_new (panel_client);

  window = mpl_panel_gtk_get_window (MPL_PANEL_GTK (panel_client));
  pane = dalston_volume_applet_get_pane (volume_applet);
  gtk_container_add (GTK_CONTAINER (window), pane);
  gtk_widget_modify_bg (window, GTK_STATE_NORMAL, &gtk_widget_get_style (window)->white);

  gtk_signal_connect (GTK_OBJECT (window), "visibility-notify-event",
                     (GtkSignalFunc) visibility_changed, NULL);
  gtk_signal_connect (GTK_OBJECT (window), "enter-notify-event",
                     (GtkSignalFunc) mouse_enter, NULL);
  gtk_signal_connect (GTK_OBJECT (window), "leave-notify-event",
                     (GtkSignalFunc) mouse_leave, NULL);

  gtk_main ();
}
